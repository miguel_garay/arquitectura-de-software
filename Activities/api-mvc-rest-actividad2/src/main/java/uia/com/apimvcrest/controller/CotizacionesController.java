package uia.com.apimvcrest.controller;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;
import uia.com.apimvcrest.compras.Comprador;
import uia.com.apimvcrest.compras.InfoComprasUIA;
import uia.com.apimvcrest.modelo.CotizacionModelo;
import uia.com.apimvcrest.servicio.CotizacionServicio;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

@RestController
public class CotizacionesController
{
    private CotizacionServicio servicioCotizaciones = new CotizacionServicio();
    private Comprador ordenCompra  = new Comprador();

    public CotizacionesController() throws IOException {
    }

    @GetMapping("/cotizaciones")
    public ArrayList<CotizacionModelo> cotizaciones()
    {
        return servicioCotizaciones.getCotizaciones();
    }

    @GetMapping("/cotizacion/{id}")
    public CotizacionModelo cotizacionById(@PathVariable int id)
    {
        return servicioCotizaciones.getCotizacion(id);
    }

     // Annotation
    @DeleteMapping("/remove-cotizacion/{id}")
     // Method
    public ArrayList<CotizacionModelo> deleteById(@PathVariable int id) 
    {

        return servicioCotizaciones.removeCotizacion(id);

    }
     @PutMapping("/cotizacion/{id}")
    public ArrayList<CotizacionModelo> updateCotizacionById(@PathVariable int id, @RequestBody ItemComprasUIAModelo cotizacion)
    {
        return  servicioCotizaciones.updateCotizacion(id, cotizacion);
    }

}

