package uia.com.apimvcrest.controller;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;
import uia.com.apimvcrest.modelo.PeticionOrdenCompraModelo;
import uia.com.apimvcrest.servicio.PeticionOrdenCompraServicio;

import java.io.IOException;
import java.util.ArrayList;


@RestController
public class PeticionOrdenCompraController
{
    private PeticionOrdenCompraServicio servicioOrdenCompra = new PeticionOrdenCompraServicio();


    public PeticionOrdenCompraController() throws IOException
    {
    }
    @GetMapping("/ordenes-compra")
    public ArrayList<PeticionOrdenCompraModelo> ordenesCompra()
    {
        return servicioOrdenCompra.getOrdenesCompra();
    }

    @GetMapping("/orden-compra/{id}")
    public ResponseEntity<PeticionOrdenCompraModelo> ordenCompraById(@PathVariable int id)
    {
        return (ResponseEntity<PeticionOrdenCompraModelo>) servicioOrdenCompra.getOrdenCompra(id);
    }
}
