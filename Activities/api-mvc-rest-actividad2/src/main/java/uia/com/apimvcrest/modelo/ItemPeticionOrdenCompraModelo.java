package uia.com.apimvcrest.modelo;

public class ItemPeticionOrdenCompraModelo
{
    private  String type = "";
    private int id = 0;
    private String name = "";
    private String codigo = "";
    private int unidad = 1;
    private int cantidad = 0;
    private int vendedor = 0;
    private String descripcion = "";
    private int pedidoProveedor = 0;
    private int clasificacion = 0;


    public ItemPeticionOrdenCompraModelo(int cantidad, String name, String codigo, int vendedor, String descripcion, int clasificacion, int pedidoProveedor)
    {
        this.type = type;
        this.name = name;
        this.codigo = codigo;
        this.cantidad = cantidad;
        this.vendedor = vendedor;
        this.descripcion = descripcion;
        this.pedidoProveedor = pedidoProveedor;
        this.clasificacion = clasificacion;
    }

    public void print()
    {
        System.out.println(
                " \n\ttype:\t"+this.type
                        +" \n\tid:\t"+this.id
                        +" \n\tname:\t"+this.name
                        +" \n\tcodigo:\t"+this.codigo
                        +" \n\tunidad:\t"+this.unidad
                        +" \n\tcantidad:\t"+this.cantidad
                        +" \n\tvendedor:\t"+this.vendedor
                        +" \n\tdescripcion:\t"+this.descripcion
                        +" \n\tpedidoProveedor:\t"+this.pedidoProveedor
                        +" \n\tclasificacion:\t"+this.clasificacion

        );
    }

    public ItemPeticionOrdenCompraModelo(String type, int id, String name, String codigo, int unidad, int cantidad, int vendedor, String descripcion, int pedidoProveedor, int clasificacion) {
        this.type = type;
        this.id = id;
        this.name = name;
        this.codigo = codigo;
        this.unidad = unidad;
        this.cantidad = cantidad;
        this.vendedor = vendedor;
        this.descripcion = descripcion;
        this.pedidoProveedor = pedidoProveedor;
        this.clasificacion = clasificacion;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public int getUnidad() {
        return unidad;
    }

    public void setUnidad(int unidad) {
        this.unidad = unidad;
    }

    public int getCantidad() {
        return cantidad;
    }

    public void setCantidad(int cantidad) {
        this.cantidad = cantidad;
    }

    public int getVendedor() {
        return vendedor;
    }

    public void setVendedor(int vendedor) {
        this.vendedor = vendedor;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public int getPedidoProveedor() {
        return pedidoProveedor;
    }

    public void setPedidoProveedor(int pedidoProveedor) {
        this.pedidoProveedor = pedidoProveedor;
    }

    public int getClasificacion() {
        return clasificacion;
    }

    public void setClasificacion(int clasificacion) {
        this.clasificacion = clasificacion;
    }
}
